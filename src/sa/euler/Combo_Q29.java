package sa.euler;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class Combo_Q29 {
    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();

        Set<BigInteger> set = new HashSet<BigInteger>();
        BigInteger temp = BigInteger.ONE;
        for (long i = 2; i < 101; i++) {
            for (int j = 2; j < 101; j++) {
                temp = BigInteger.valueOf(i);
                temp = temp.pow(j);
                set.add(temp);
            }
        }

        System.out.println(set.size());
        end = System.currentTimeMillis();
        System.out.println((end - start) / 1000);
    }

}
