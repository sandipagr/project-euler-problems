package sa.euler;

/** 
 * Calculates total number of Sundays on the first of the month during the 
 * twentieth century  (1 Jan 1901 to 31 Dec 2000) given 1 Jan 1900 was a Monday
 *  (http://projecteuler.net/problem=19)
 *  
 * @author sagrawal
 * 
 */
public class SundayMonth_Q19 {
    public int[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    public int calculateSundays(int startYear, int endYear){
        int numOfSundays = 0;
        int daysCalculated = 1;
        while(startYear < endYear + 1){
            for(int i=0; i<12; i++){
                if(isSunday(daysCalculated)){
                    numOfSundays++;
                }                
                int monthDays = getDaysInMonth(startYear, i);
                daysCalculated += monthDays;
            }
            startYear++;
        }
        return numOfSundays;
    }
    
    private boolean isSunday(int days) {
        int modifiedDays = days + 366 ;
        if(modifiedDays % 7 == 1){
            return true;
        }
        return false;
    }

    public int getDaysInMonth(int year, int monthIndex){
        if(monthIndex == 1 && isLeap(year)){
            return 29;
        }
        return daysInMonth[monthIndex];
    }
    
    public boolean isLeap(int year){
        if(year%4 == 0 && year%400 != 0){
            return true;
        }
        return false;
    }
    
    public static void main(String[] args) {
        SundayMonth_Q19 inst = new SundayMonth_Q19();
        int numSundays = inst.calculateSundays(1901, 2000);
        System.out.println("Sundays Count : " + numSundays);
    }

}
