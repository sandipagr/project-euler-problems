package sa.euler;

public class SameDigits_Q52 {

    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();

        boolean a = true;
        int index = 10;
        while (a) {
            String num = Integer.toString(index);
            String num6 = Integer.toString(6 * index);

            if (num.length() == num6.length()) {

                char[] char1 = num.toCharArray();
                java.util.Arrays.sort(char1);
                String sorted1 = new String(char1);
                int count = 0;
                for (int i = 2; i < 7; i++) {
                    char[] char6 = Integer.toString(i * index).toCharArray();
                    java.util.Arrays.sort(char6);
                    String sorted6 = new String(char6);
                    if (!sorted1.equals(sorted6)) {
                        index++;
                        break;
                    } else {
                        count++;
                    }
                }
                if (count == 5) {
                    a = false;
                    System.out.println("Ans: " + num);
                }

            } else {
                int len = num.length();
                String str = "1";
                for (int i = 0; i < len; i++) {
                    str = str + "0";
                }
                index = Integer.parseInt(str);
            }

        }

        end = System.currentTimeMillis();
        System.out.println("Time Taken (in ms) = " + (end - start));

    }

}
