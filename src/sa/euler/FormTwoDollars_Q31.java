package sa.euler;

public class FormTwoDollars_Q31 {
    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        int count = 2;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 5; j++) {
                for (int k = 0; k < 11; k++) {
                    for (int l = 0; l < 21; l++) {
                        for (int m = 0; m < 41; m++) {
                            for (int n = 0; n < 101; n++) {
                                for (int o = 0; o < 201; o++) {
                                    int temp = i * 100 + j * 50 + k * 20 + l * 10 + m * 5 + n * 2 + o;
                                    if (temp == 200) {
                                        count++;
                                    } else {
                                        if (temp > 200)
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Answer : " + count);
        end = System.currentTimeMillis();
        System.out.println("Time Taken (in ms) = " + (end - start));

    }
}
