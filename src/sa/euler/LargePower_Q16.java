package sa.euler;

/*
 * 215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 * What is the sum of the digits of the number 21000?
 */

public class LargePower_Q16 {

    public String addString(String a) {

        int carry = 0;
        int tempsum = 0;
        String ans = "";
        for (int i = a.length() - 1; i > -1; i--) {
            tempsum = carry + 2 * Integer.parseInt(a.substring(i, i + 1));
            String str = Integer.toString(tempsum);
            int digitToAdd = Integer.parseInt(str.substring(str.length() - 1, str.length()));
            ans = digitToAdd + ans;
            if (str.length() > 1) {
                carry = Integer.parseInt(str.substring(0, str.length() - 1));
            } else {
                carry = 0;
            }
        }

        return (carry + ans);

    }

    public static void main(String[] args) {
        LargePower_Q16 inst = new LargePower_Q16();
        long pow = 1000;
        long steps = 50;
        int numSteps = (int) (pow - steps);

        String value = Long.toString((long) Math.pow(2, steps));

        for (int i = 0; i < numSteps; i++) {
            value = inst.addString(value);
        }

        int sum = 0;
        for (int i = 0; i < value.length(); i++) {
            sum = sum + Integer.parseInt(value.substring(i, i + 1));
        }

        System.out.println(sum);
    }

}
