package sa.euler;

public class Truncable_Q37 {

    public boolean isPrime(int i) {
        int temp = (int) Math.sqrt(i);
        for (int j = 2; j <= temp; j++) {
            if (i % j == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Truncable_Q37 inst = new Truncable_Q37();
        long start, end;
        start = System.currentTimeMillis();
        String first = "357";
        String middle = "1379";
        int sum = 23 + 73 + 37 + 53;
        String last = "37";
        int flag = 0;
        for (int i = 309; i < 799998; i = i + 2) {
            flag = 0;
            String str = Integer.toString(i);
            if ((first.indexOf(str.substring(0, 1)) < 0) || (last.indexOf(str.substring(str.length() - 1)) < 0)) {
                continue;
            }
            String part = str.substring(1, str.length() - 1);
            for (int j = 0; j < part.length(); j++) {
                if (middle.indexOf(part.substring(j, j + 1)) < 0) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                if (inst.isPrime(i)) {
                    int flag2 = 0;
                    for (int j = 2; j < str.length(); j++) {
                        if (!inst.isPrime(Integer.parseInt(str.substring(0, j)))
                                || (!inst.isPrime(Integer.parseInt(str.substring(j - 1, str.length()))))) {
                            flag2 = 1;
                            break;
                        }
                    }
                    if (flag2 == 0) {
                        sum += i;
                    }
                }
            }

        }
        System.out.println("Ans = " + sum);
        end = System.currentTimeMillis();
        System.out.println((end - start));

    }

}
