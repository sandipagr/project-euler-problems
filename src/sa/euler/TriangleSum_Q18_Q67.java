package sa.euler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TriangleSum_Q18_Q67 {
    public static void main(String[] args) {
        try {
            ArrayList<int[]> array = new ArrayList<int[]>();
            BufferedReader reader = new BufferedReader(new FileReader(new File("resources/triangle.txt")));
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] temps = line.split(" ");
                int[] toAdd = new int[temps.length];
                for (int i = 0; i < temps.length; i++) {
                    toAdd[i] = Integer.parseInt(temps[i]);
                }
                array.add(toAdd);
            }
            int[] b = array.get(array.size() - 1);
            for (int i = array.size() - 2; i > -1; i--) {
                int[] a = array.get(i);
                int[] mainArray = new int[a.length];
                for (int j = 0; j < a.length; j++) {
                    int tempa = a[j];
                    if (tempa + b[j] > tempa + b[j + 1]) {
                        mainArray[j] = tempa + b[j];
                    } else {
                        mainArray[j] = tempa + b[j + 1];
                    }
                }
                b = mainArray;
            }

            System.out.println(b[0]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
