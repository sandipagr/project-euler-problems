package sa.euler;

public class BackTracGrid_Q15 {

    public static void main(String[] args) {
        long start, end;
        int grid = 20;
        start = System.currentTimeMillis();
        long board[][] = new long[21][21];

        // Initialize the 21 x 21 grid
        board[grid][grid] = 0;
        for (int i = 0; i < grid; i++) {
            board[grid][i] = 1;
            board[i][grid] = 1;
        }

        for (int i = grid - 1; i > -1; i--) {
            for (int j = grid - 1; j > -1; j--) {
                board[j][i] = board[j + 1][i] + board[j][i + 1];
            }
        }

        System.out.println("Answer : " + board[0][0]);
        end = System.currentTimeMillis();
        System.out.println((end - start));
    }

}
