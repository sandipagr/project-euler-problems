package sa.euler;

import java.util.ArrayList;

public class Quadratic_Q27 {

    public boolean isPrime(int i) {

        int temp = (int) Math.sqrt(i);
        for (int j = 2; j <= temp; j++) {
            if (i % j == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        ArrayList<Integer> primes = new ArrayList<Integer>();
        Quadratic_Q27 inst = new Quadratic_Q27();
        for (int i = 2; i < 1000; i++) {
            if (inst.isPrime(i) == true) {
                primes.add(i);
            }
        }

        int maxCount = 1;
        int aval = 0;
        int bval = 0;
        int len = primes.size();

        for (int i = -999; i < 1000; i = i + 2) {
            for (int j = 0; j < len; j++) {

                int b = primes.get(j);
                int n = 0;
                int count = 0;
                boolean a = true;

                while (a) {
                    int temp = n * n + n * i + b;
                    if ((temp > 1) && inst.isPrime(temp)) {
                        count++;
                        n = n + 1;
                    } else {
                        if (count > maxCount) {
                            maxCount = count;
                            aval = i;
                            bval = b;
                            a = false;
                        }
                        a = false;
                    }
                }

            }
        }

        System.out.println("Ans = " + aval * bval);
        end = System.currentTimeMillis();
        System.out.println((end - start));

    }
}
