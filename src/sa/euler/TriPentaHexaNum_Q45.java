package sa.euler;

import java.util.ArrayList;

public class TriPentaHexaNum_Q45 {
    
    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        ArrayList<Integer> triArray = new ArrayList<Integer>();
        ArrayList<Integer> pentaArray = new ArrayList<Integer>();
        ArrayList<Integer> hexArray = new ArrayList<Integer>();
        
        int tri = 286;
        int penta = 166;
        int hex = 144;
        
        int triVal = tri * (tri + 1) / 2;
        triArray.add(triVal);
        
        int pentaVal = penta * (3 * penta - 1) / 2;
        pentaArray.add(pentaVal);
        
        int hexVal = hex * (2 * hex - 1);
        hexArray.add(hexVal);

        int tritoAdd = triVal - 40755;
        int pentatoAdd = pentaVal - 40755;
        int hextoAdd = hexVal - 40755;
        
        boolean state = true;
        while (state) {
            tritoAdd += 1;
            pentatoAdd += 3;
            hextoAdd += 4;

            triVal += tritoAdd;
            triArray.add(triVal);
            pentaVal += pentatoAdd;
            pentaArray.add(pentaVal);
            hexVal += hextoAdd;
            hexArray.add(hexVal);

            if (pentaArray.contains(triVal) && hexArray.contains(triVal)) {
                System.out.println("Answer = " + triVal);
                state = false;
            }
        }
        end = System.currentTimeMillis();
        System.out.println("Time Taken (in sec) = " + (end - start) / 1000);
    }
}
