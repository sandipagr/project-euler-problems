package sa.euler;

public class Palindrome_Q36 {
    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        long sumReal = 0;
        String str;
        for (int i = 1; i < 1000000; i++) {
            str = Integer.toString(i);

            if (str.charAt(0) != (str.charAt(str.length() - 1))) {
                continue;
            }

            String reverse = "";
            for (int j = str.length() - 1; j >= 0; j--) {
                reverse = reverse + str.charAt(j);
            }

            if (str.equals(reverse)) {
                String binStr = Integer.toBinaryString(i);
                String binreverse = "";
                for (int j = binStr.length() - 1; j >= 0; j--) {
                    binreverse += binStr.charAt(j);
                }
                if (binStr.equals(binreverse)) {
                    sumReal = sumReal + i;
                }
            }
        }

        System.out.println("Answer : " + sumReal);
        end = System.currentTimeMillis();
        System.out.println((end - start) / 1000);
    }
}
