package sa.euler;

import java.util.HashSet;
import java.util.Set;

public class LargestPandigitalPrime_Q41 {

    public boolean isPrime(int i) {
        int temp = (int) Math.sqrt(i);
        for (int j = 2; j <= temp; j++) {
            if (i % j == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        long start, end;
        start = System.currentTimeMillis();
        LargestPandigitalPrime_Q41 inst = new LargestPandigitalPrime_Q41();
        boolean temp = true;
        int num = 7654321;
        for (int i = num; i > 1; i = i - 2) {

            String str = Integer.toString(i);
            if ((str.indexOf("0") > 0) || (str.indexOf("8") > 0) || (str.indexOf("9") > 0)) {
                continue;
            }
            Set<String> numbers = new HashSet<String>();
            for (int j = 0; j < str.length(); j++) {
                temp = numbers.add(str.substring(j, j + 1));
                if (!temp) {
                    break;
                }
            }

            if (temp && inst.isPrime(i)) {
                num = i;
                break;
            }

        }

        System.out.println("Ans = " + num);
        end = System.currentTimeMillis();
        System.out.println((end - start));

    }

}
