package sa.euler;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CircularPrime_Q35 {
    Set<Integer> set = new HashSet<Integer>();
    int sum = 0;

    public CircularPrime_Q35() {
        for (int i = 100; i < 1000000; i++) {
            int temp = (int) Math.sqrt(i);
            int flag = 0;
            for (int j = 2; j < temp; j++) {
                if (i % j == 0) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                set.add(i);
            }
        }
        start();
    }

    public void start() {
        Iterator<Integer> iter = set.iterator();
        sum = 0;
        while (iter.hasNext()) {
            sum += checkCircle(Integer.toString(iter.next()));
        }
    }

    public int checkCircle(String n) {
        int len = n.length();
        for (int i = 1; i < len; i++) {
            String str = n.substring(i) + n.substring(0, i);
            if (!set.contains(Integer.parseInt(str))) {
                return 0;
            }
        }
        return 1;
    }

    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        CircularPrime_Q35 inst = new CircularPrime_Q35();
        System.out.println("Number of Circular Primes = " + (inst.sum + 13));
        end = System.currentTimeMillis();
        System.out.println((end - start) / 1000);
    }
}
