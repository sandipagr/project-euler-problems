package sa.euler;

import java.math.BigInteger;

public class SumFact_Q34 {

    public long factorial(long n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        BigInteger sum = BigInteger.ZERO;
        long[] array = new long[10];
        array[0] = 1;
        SumFact_Q34 inst = new SumFact_Q34();
        for (int i = 1; i < 10; i++) {
            array[i] = inst.factorial(i);
        }

        long max = array[9];
        for (long i = 10; i < max; i++) {
            long tempsum = 0;
            String str = Long.toString(i);
            for (int j = 0; j < str.length(); j++) {
                tempsum += array[Integer.parseInt(str.substring(j, j + 1))];
            }

            if (tempsum == i) {
                sum = sum.add(BigInteger.valueOf(i));
            }
        }

        System.out.println(sum);
        end = System.currentTimeMillis();
        System.out.println((end - start) / 1000);
    }
}
