package sa.euler;

import java.util.*;

/*The following iterative sequence is defined for the set of positive integers:

 n  n/2 (n is even)
 n  3n + 1 (n is odd)

 Using the rule above and starting with 13, we generate the following sequence:

 13  40  20  10  5  16  8  4  2  1
 It can be seen that this sequence (starting at 13 and finishing at 1) 
 contains 10 terms. Although it has not been proved yet (Collatz Problem), 
 it is thought that all starting numbers finish at 1.

 Which starting number, under one million, produces the longest chain?

 NOTE: Once the chain starts the terms are allowed to go above one million.
 */

public class LongestChain_Q14 {

    HashMap<Long, Integer> map = new HashMap<Long, Integer>();

    public int start() {
        int currentCount = 0;
        int maxNow = 1;
        int maxCount = 1;
        for (Long i = (long) 1; i < 1000000; i++) {
            currentCount = chainNum(i);
            System.out.println(i + " : " + currentCount);
            map.put(i, currentCount);
            if (currentCount > maxCount) {
                maxNow = i.intValue();
                maxCount = currentCount;
            }
        }
        return maxNow;
    }

    public int chainNum(Long num) {
        int count = 1;
        while (num != 1) {
            
            if(map.containsKey(num)){
                    count = count + map.get(num);
                    return count;
            } else {
                if (num % 2 == 0) {
                    num = num / 2;
                } else {
                    num = (3 * num) + 1;
                }
            }
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        long start, end;
        start = System.currentTimeMillis();
        LongestChain_Q14 inst = new LongestChain_Q14();
        int maxNow = inst.start();
        System.out.println("Answer : " + maxNow);
        end = System.currentTimeMillis();
        System.out.println((end - start) / 1000);
    }
}
